import React, { Component } from 'react';
import { Sparklines, SparklinesLine } from 'react-sparklines';
import _ from 'lodash';
import moment from 'moment';
import aggregator from './utils/aggregator';
import cr from './utils/commaReplacer';
import cx from 'classnames';

class TaskView extends Component {
  state = {
    showWorklog: true,
    showPopup: true,
    showScreenshots: true,
    record: null,
    activePopIndex: null,
    activeIndex: null,
  };

  toggleWorklog = () => {
    this.setState((prevState) => {
      return { showWorklog: !prevState.showWorklog };
    });
  }

  onMouseMove = (record, activeIndex) => {
    return (...args) => {
      this.setState({ record, activeIndex, activePopIndex: _.nth(args, 3)});
    };
  }

  onPopupLeave = () => {
    this.setState({activeIndex: null});
  }

  togglePopup = () => {
    this.setState((prevState) => {
      return { showPopup: !prevState.showPopup };
    });
  }

  toggleScreenshot = () => {
    this.setState((prevState) => {
      return { showScreenshots: !prevState.showScreenshots };
    });
  }

  isScreenUploaded(task, id) {
    return _.some(task.uploadScreens,(u) => {
      return _.replace(_.nth(_.get(u, 'requestData.uuid'), 0), /\'/g, '') === id;
    });
  }

  render() {
    const { taskId, records } = this.props;
    const { record, activePopIndex, activeIndex, showScreenshots, showPopup } = this.state;
    const task = records[taskId];
    const workingTaskId = _.get(task, 'start.taskId');
    const taskDuration = _.get(_.last(_.get(task, 'tasksTime')), 'friendlyDuration');
    const companyTimezone = _.get(_.last(task, 'companyTime'), 'CompanyTimeZoneName');
    let prevTime = null;

    return (
      <div className="timeline__task_view">
        <div className="task__duration">
          <div className="task__duration__time-container">
            <div className="task__duration__time">
              {taskDuration && taskDuration.charAt(0)}
            </div>
            <div className="task__duration__time">
              {taskDuration && taskDuration.charAt(1)}
            </div>
            <div className="task__duration__time">
              {taskDuration && taskDuration.charAt(2)}
            </div>
            <div className="task__duration__time">
              {taskDuration && taskDuration.charAt(3)}
            </div>
            <div className="task__duration__time">
              {taskDuration && taskDuration.charAt(4)}
            </div>
            <div className="task__duration__time">
              {taskDuration && taskDuration.charAt(5)}
            </div>
            <div className="task__duration__time">
              {taskDuration && taskDuration.charAt(6)}
            </div>
            <div className="task__duration__time">
              {taskDuration && taskDuration.charAt(7)}
            </div>
          </div>
        </div>
        <div className="task__timings">
          <div className="task__timings__start">
            {moment(_.get(task, 'start.time')).format('h:mm:ss a')}
          </div>
          <div>
            {'-'}
          </div>
          <div className="task__timings__end">
            {moment(_.get(task, 'stop.time')).format('h:mm:ss a')}
          </div>
        </div>
        <div className="task__uploaded__timings">
          <div className="task__uploaded__header" onClick={this.toggleWorklog}>Uploaded Worklog</div>
          {
            this.state.showWorklog && (_.map(task.uploadedTime, (time, i) => {
              const requestData = time.requestData;
              const index = _.indexOf(requestData.task, workingTaskId);
              const startTime = cr(requestData.start_time[index]);
              const endTime = cr(requestData.end_time[index]);
              let isAfter = false;
              if (i > 0) {
                isAfter = moment(prevTime).isAfter(endTime);
              }
              prevTime = endTime;
              const tz = cr(requestData.end_tz[index]);
              return (
                <div className="task__uploaded__time" key={i}>
                  <div className="start-time">
                    <div className="start-time__humanized">{moment(startTime+tz).format('hh:mm:ss a')}</div>
                    <div className="start-time__data">{startTime}</div>
                  </div>
                  <div className="end-time">
                    <span className={cx({"start-time__humanized": true, "end-time__date-isafter": isAfter})}>{moment(endTime+tz).format('hh:mm:ss a')}</span>
                    <span className={cx({"end-time__data": true, "end-time__date-isafter": isAfter})}>{endTime}</span>
                  </div>
                </div>
              );
            }))
          }
        </div>
        {
          !_.isEmpty(task.countdownpopup) && <div className="task__uploaded__popup">
          <div className="task__uploaded__header" onClick={this.togglePopup}>Popup Activity</div>
          <div className="task__uploaded__graph">
          {
            showPopup && _.map(task.countdownpopup, (popup, index) => {
              return (
                <div className="task__uploaded__graph__popup" key={'popup-' + index} onMouseLeave={this.onPopupLeave}>
                  <div>
                    <Sparklines data={_.get(popup, 'activity.activityFactor')} width={100} height={20} margin={5}>
                      <SparklinesLine style={{ fill: "red", stroke: "red", strokeWidth: '0.25px'}}  color="#777" onMouseMove={this.onMouseMove(task.countdownpopup[index], index)} points={[{x: 1, y: 2}, {x: 3, y: 5}]}/>
                    </Sparklines>
                  </div>
                  {record && activeIndex === index && (
                    <div className="activity-container">
                      <div className="activity">
                        <div className="keyboard-icon"/>
                        <div>{_.nth(_.get(record, 'activity.keyStrokes'), activePopIndex)}</div>
                      </div>
                      <div className="activity">
                        <div className="click-icon"/>
                        <div>{_.nth(_.get(record, 'activity.mouseClicks'), activePopIndex)}</div>
                      </div>
                      <div className="activity">
                        <div className="mouse-icon"/>
                        <div>{_.nth(_.get(record, 'activity.mouseMovements'), activePopIndex)}</div>
                      </div>
                      <div className="activity activity-factor">
                        <div>{_.nth(_.get(record, 'activity.activityFactor'), activePopIndex)}</div>
                      </div>
                    </div>
                  )}
                  <div className="popup-time-container">
                    <div>{moment(popup.showPopup).format('hh:mm:ss a')}</div>
                    <div>{moment(popup.onTimeOut).format('hh:mm:ss a')}</div>
                  </div>
                </div>
              );
            })
          }
          </div>
        </div>
      }
        <div className="task__screenshots">
          <div className="task__uploaded__header" onClick={this.toggleScreenshot}>Screenshots Activity</div>
          <div>
            {
              showScreenshots && _.map(task.screenshots, (s, i) => {
                const shotId = _.replace(_.get(s, 'info.shotId'), /\'/g, '');
                return (shotId && (<div key={(shotId + '-shot-'+  i)} className="task-screenshot">
                  <span className="screenshot-box">
                    {
                      this.isScreenUploaded(task, shotId) && <div className="screenshot-tick"/>
                    }
                  </span>
                  <div className="screenshot-activity-container">
                    <div className="screenshot-activity">
                      <div className="keyboard-icon"/>
                      <div>{_.get(s, 'info.keyStrokes')}</div>
                    </div>
                    <div className="screenshot-activity">
                      <div className="click-icon"/>
                      <div>{_.get(s, 'info.mouseClicks')}</div>
                    </div>
                    <div className="screenshot-activity">
                      <div className="mouse-icon"/>
                      <div>{_.get(s, 'info.mouseMovements')}</div>
                    </div>
                  </div>
                  <span>Interval: {s.screenShotInterval} min</span>
                  <span>{moment(_.get(s, 'time')).format('hh:mm:ss a')}</span>
                  <span>{shotId}</span>
                </div>))
              })
            }
          </div>
        </div>
      </div>
    );
  }
}

export default TaskView;