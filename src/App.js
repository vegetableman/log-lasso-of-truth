import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import moment from 'moment';
import logo from './logo.svg';
import _ from 'lodash';
import cx from 'classnames';

import './App.css';
import Timeline from './Timeline'

class App extends Component {
  state = {
    versions: [],
    showVersions: false,
    selectedVersion: null,
    selectedDate: null,
    indexLogData: null,
    indexLogDates: null,
    indexLogs: null,
    showDetails: false,
    cacheDir: '/Users/viven/Library/Application Support/Electron',
  }

  onDrop = (files) => {
    const file = files[0];
    const self = this;

    let formData = new FormData();
    formData.append('file', file);

    let request = new XMLHttpRequest();
    request.open("POST", "http://localhost:8000/files");
    request.send(formData);
    request.onreadystatechange = function() {
      if (request.readyState === 4 && request.status === 200) {
        const versions = JSON.parse(request.responseText).versions;
        self.setState({
          versions,
          selectedVersion: _.nth(versions, 0),
          showVersions: true,
        });
      }
    };
  }

  handleSubmit = () => {
    let request = new XMLHttpRequest();
    const self = this;
    request.open("GET", "http://localhost:8000/logs?version=" + this.state.selectedVersion);
    request.send();
    request.onreadystatechange = function() {
      if (request.readyState === 4 && request.status === 200) {
        const response = JSON.parse(request.responseText);
        console.log(_.keys(response.indexLogData));
        self.setState({
          ...response,
          indexLogDates: _.sortBy(_.keys(response.indexLogData)),
        });
      }
    }; 
  }

  handleRefresh = () => {
    let request = new XMLHttpRequest();
    const self = this;
    const { cacheDir } = self.state;
    console.log('cacheDir: ', cacheDir);
    request.open("POST", "http://localhost:8000/dir");
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(JSON.stringify({path: cacheDir}));
    request.onreadystatechange = function() {
      if (request.readyState === 4 && request.status === 200) {
        const versions = JSON.parse(request.responseText).versions;
        self.setState({
          versions,
          selectedVersion: _.nth(versions, 0),
          showVersions: true
        });
      }
    }; 
  }

  handleChange = (event) => {
    this.setState({selectedVersion: event.target.value});
  }

  handleChangeDir = (e) => {
    this.setState({cacheDir: e.target.value});
  }

  handleDateSelection = (d) => {
    return () => {
      this.setState({selectedDate: d, showDetails: true});
    }
  }

  getIndexLog() {
    const { indexLogData, selectedDate } = this.state;
    return _.get(indexLogData, selectedDate);
  }

  getRawView(indexLogRecords) {
    return _.map(indexLogRecords, (d) => {
      return (
        <div className={cx({
          "log": true,
          "log--error": d.level === 50,
        })} 
        key={'log-' + d._logid}>
          <span className="log__time">
            {moment(d.time).format('h:mm:ss.SSSS a')}
          </span>
          <span className="log__msg">
            {d.msg}
          </span>
        </div>
      )
    })
  }

  render() {
    const { versions, indexLogDates, indexLogData, selectedDate, cacheDir, showVersions, showDetails } = this.state;
    const indexLogRecords = this.getIndexLog();
    return (
      <div className="App">
        <div className="hero">
          <div className="hero-title">Logging Lasso of Truth</div>
        </div>
        <div className="main">
          <Tabs>
            <TabList className="file-folder-tabs">
              <Tab>File</Tab>
              <Tab>Folder</Tab>
            </TabList>
            <TabPanel>
              <div className="drop-container">
                <Dropzone ref={(node) => { this.dropzoneRef = node; }} onDrop={this.onDrop}>
                    <p>Drop asar</p>
                </Dropzone>
                <button type="button" onClick={() => { this.dropzoneRef.open() }} className="drop-button ll-button">
                    Open File Dialog
                </button>
              </div>
            </TabPanel>
            <TabPanel>
              <div className="dir-container">
                <input className="dir-input" type="input" value={cacheDir} onChange={this.handleChangeDir}/>
                <input className="dir-submit ll-button" type="button" value="submit" onClick={this.handleRefresh}/>
              </div>
            </TabPanel>
          </Tabs>
          {showVersions && (
           <div className="version-select-container">
              <select onChange={this.handleChange} className="version-select">
                {
                  _.map(versions, (v, index) => {
                    return <option key={index} value={v}>{v}</option>
                  })
                }
              </select>
              <input className="ll-button version-btn" type="button" value="submit" onClick={this.handleSubmit}/>
            </div>
          )}
         <div className="days">
            {
              _.map(indexLogDates, (d, i) => {
                const m = moment(d);
                return (
                  <div key={`day-${i}`} className={cx({'day': true, 'selected-day': d === selectedDate})} onClick={this.handleDateSelection(d)}>
                    <div className="day-date">{m.format('DD')}</div>
                    <div className="day-month">
                      <span>{m.format('MMMM')}</span>
                      <span className="day-year">{m.format('YYYY')}</span>
                    </div>
                  </div>
                )
              })
            }
          </div>
          {showDetails && (
          <div className="details">
            <Tabs>
              <TabList className="details-tabs">
                <Tab>Timeline</Tab>
                <Tab>Raw</Tab>
              </TabList>

              <TabPanel>
                <Timeline
                  selectedDate={selectedDate}
                  indexLogRecords={indexLogRecords}
                />
              </TabPanel>
              <TabPanel>
                <h2>Any content 3</h2>
                <div className="raw-logs">
                  {this.getRawView(indexLogRecords)}
                </div>
              </TabPanel>
            </Tabs>
          </div>)}
        </div>
      </div>
    );
  }
}

export default App;
