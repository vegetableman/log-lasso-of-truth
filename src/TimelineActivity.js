import React, { Component } from 'react';
import moment from 'moment';
import _ from 'lodash';
import cx from 'classnames';

import taskCounter from './utils/taskCounter';

const MIN_BLOCK_HEIGHT = 25;
const TIMELINE_ITEM_HEIGHT = 80;

let taskIndex = 0;

const calculateBlockHeight = (minutes, height) => {
  const frac = (minutes * 0.5 / 30)
  return height * frac; 
}

class TimelineActivity extends Component {
  constructor(props) {
    super(props);
    this.taskCounter = taskCounter.getInstance();
  }

  renderActivity(nextDateTime, prevDateTime, timeOrder, taskOrder, records, timelineHeight, blockArr = []) {
    const mNextDateTime = moment(nextDateTime);
    const mPrevDateTime = moment(prevDateTime);
    const taskIndex = this.taskCounter.getIndex();

    // If reached the end of timeOrder array, return
    if (!timeOrder || !timeOrder[taskIndex]) {
      return !_.isEmpty(blockArr) ? <React.Fragment>{blockArr}</React.Fragment> : null;
    }

    const mtaskStartTime = moment(_.get(timeOrder[taskIndex], 'start'));

    // if days are not equal
    if (mNextDateTime.format('D') !== mtaskStartTime.format('D')) {
      this.taskCounter.increment();
      return this.renderActivity(nextDateTime, prevDateTime, timeOrder, taskOrder, records, timelineHeight, blockArr);
    }
    
    // next hour greater than task start time
    else if (mNextDateTime.diff(mtaskStartTime) > 0) {
      const mtaskEndTime = moment(_.get(timeOrder[taskIndex], 'stop'));
      const hourDiff = mtaskEndTime.diff(mtaskStartTime, 'hours');
      const minuteDiff = mtaskEndTime.diff(mtaskStartTime, 'minutes');

      // console.log('mtaskStartTime: ', mtaskStartTime.format('MMMM Do YYYY, h:mm:ss a'), ' ', 'mtaskEndTime: ', mtaskEndTime.format('MMMM Do YYYY, h:mm:ss a'), ' ', mNextDateTime.format('MMMM Do YYYY, h:mm:ss a'), taskIndex);

      const blockHeight = calculateBlockHeight(minuteDiff, timelineHeight || TIMELINE_ITEM_HEIGHT);
      const timelineHeight = blockHeight + TIMELINE_ITEM_HEIGHT;

      // Add empty pre-block
      if (timeOrder[taskIndex].prevDiff && !this.taskCounter.getOverflow()) {
        const emptyBlockHeight = calculateBlockHeight(timeOrder[taskIndex].prevDiff, TIMELINE_ITEM_HEIGHT);
        blockArr.push(
          <div key={taskOrder[taskIndex]+'-prev-empty'} style={{'flex': `0 1 ${emptyBlockHeight}px`}} className="timeline__empty-block" />
        );
      }

      // Add task block
      if (timeOrder[taskIndex].overflow && this.taskCounter.getOverflow() && mNextDateTime.diff(mtaskEndTime) > 0) {
        const overflowBlockHeight = calculateBlockHeight(_.get(timeOrder[taskIndex], 'overflow.diff'), TIMELINE_ITEM_HEIGHT);
        this.taskCounter.setOverflow(false);
        blockArr.push(
          <div key={taskOrder[taskIndex]} style={{'flex': `0 1 ${overflowBlockHeight}px`}} className="timeline__block timeline__block--overflow"/>
        );  
      }
      else if (timeOrder[taskIndex].overflow && this.taskCounter.getOverflow() && mNextDateTime.diff(mtaskEndTime) < 0) {
        blockArr.push(
          <div key={taskOrder[taskIndex]} style={{'flex': `0 1 ${TIMELINE_ITEM_HEIGHT}px`}} className="timeline__block timeline__block--overflow"/>
        );  
      }
      else {
        if (timeOrder[taskIndex].overflow && !this.taskCounter.getOverflow()) {
          this.taskCounter.setOverflow(true);
        }

        if (timeOrder[taskIndex].welcomebackpopup) {
          console.log('welcomebackpopup');
        }

        blockArr.push(
          <div 
            key={taskOrder[taskIndex]}
            style={{'flex': `0 1 ${blockHeight}px`}}
            className={cx({
              'timeline__block': true,
              'timeline__block--active': this.props.activeTaskId === taskOrder[taskIndex],
            })}
            onClick={this.props.onTaskClick(taskOrder[taskIndex])}
          >
            {_.replace(_.get(records[taskOrder[taskIndex]], 'start.taskName'), /\'/g, '')}
          </div>
        );
      }

     // Add empty post-block
      if (timeOrder[taskIndex].nextDiff) {
        const emptyBlockHeight = calculateBlockHeight(timeOrder[taskIndex].nextDiff,
            timelineHeight || TIMELINE_ITEM_HEIGHT);
        blockArr.push(
          <div key={taskOrder[taskIndex]+'-next-empty'} style={{'flex': `0 1 ${emptyBlockHeight}px`}} className="timeline__empty-block" />
        );
      }

      // If overflowing time, don't increment
      if (!timeOrder[taskIndex].overflow || !this.taskCounter.getOverflow()) {
        this.taskCounter.increment();
      }
      else {
        return <React.Fragment>{blockArr}</React.Fragment>;
      }

      return this.renderActivity(nextDateTime, prevDateTime, timeOrder, taskOrder, records, timelineHeight, blockArr);
    }
    else {
      return <React.Fragment>{blockArr}</React.Fragment>;
    }
  }

  render() {
    const { nextDateTime, prevDateTime, records } = this.props;
    const { timeOrder, taskOrder } = records;
    return (
      <div className="timeline__activity">
        {this.renderActivity(nextDateTime, prevDateTime, timeOrder, taskOrder, records)}
      </div>
    );
  }
}

export default TimelineActivity;