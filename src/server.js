var express = require('express');
var multer = require('multer');
var asar = require('asar');
var Lasso = require('./lasso');
var cors = require('cors');
var bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var lasso = new Lasso();

app.use(cors());

// configuring Multer to use files directory for storing files
// this is important because later we'll need to access file path
const storage = multer.diskStorage({
  destination: './files',
  filename(req, file, cb) {
    cb(null, `../../files/${Date.now()}-${file.originalname}`, (info) => {
      console.log('done');
      console.log(info);
    });
  },
});

const upload = multer({ storage });

// express route where we receive files from the client
// passing multer middleware
app.post('/files', upload.single('file'), (req, res) => {
  const newFileName = req.file.path;
  const destDir = newFileName.split('-')[0];
  asar.extractAll(newFileName, destDir);

  res.json({versions: lasso.parseDir(destDir).getVersions()});
});

app.post('/dir', (req, res) => {
  res.json({versions: lasso.parseDir(req.body.path).getVersions()});
});

app.get('/logs', (req, res) => {
  const version = req.query.version;
  const indexLogs = lasso.getIndexLogs(version);
  res.send(JSON.stringify({
    indexLogs,
    ...lasso.parseIndexLogs(indexLogs),
  }));
});

app.listen(8000, function() {
  // Log (server-side) when our server has started
  console.log("Server listening on: http://localhost:" + 8000);
});
