import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';

import aggregator from './utils/aggregator';
import taskCounter from './utils/taskCounter';
import TimelineActivity from './TimelineActivity';
import TaskView from './TaskView';
import './Timeline.css';

const hours = [
  '24',
  '01',
  '02',
  '03',
  '04',
  '05',
  '06',
  '07',
  '08',
  '09',
  '10',
  '11',
  '12',
  '13',
  '14',
  '15',
  '16',
  '17',
  '18',
  '19',
  '20',
  '21',
  '22',
  '23',
];

class Timeline extends Component {
  state = { showTask: null };

  records = {};

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedDate !== this.props.selectedDate) {
      taskCounter.getInstance().reset();
      this.setState({showTask: null});
      this.records = aggregator(nextProps.indexLogRecords);
    }
  }

  onTaskClick = (taskId) => {
    return () => {
      this.setState({showTask: taskId});
    };
  }

  renderTimeline(records, selectedDate) {
    if (!selectedDate) return null;
    const date = moment(selectedDate).format('YYYY-MM-DD');
    console.log('selectedDate: ', date);
    console.log('records: ', records);
    return (
      <div className="timeline__list">
        {
          _.map(hours, (h, i) => {
            return (
              <div className="timeline__item" key={h}>
                <span className="timeline__hour">{moment(`${date}T${h}:00:00`).format('h a')}</span>
                <TimelineActivity
                  records={records}
                  activeTaskId={this.state.showTask}
                  prevDateTime={`${date}T${hours[i]}:00:00`} 
                  nextDateTime={(hours[i+1] ? `${date}T${hours[i+1]}:00:00` : null)}
                  onTaskClick={this.onTaskClick}
                />
              </div>
            );
          })
        }
      </div>
    );
  }

  renderSidebar(records) {
    if (this.state.showTask) {
      return <TaskView taskId={this.state.showTask} records={records}/>
    }
  }

  componentWillUpdate() {
    taskCounter.getInstance().reset();
  }

  componentWillMount() {
    taskCounter.getInstance().reset();
    this.records = aggregator(this.props.indexLogRecords);
  }

  render() {
    const { selectedDate } = this.props;
    return (
      <div className="timeline">
        <div className="timeline__view">
          {this.renderTimeline(this.records, selectedDate)}
        </div>
        <div className="timeline__sidebar">
          {this.renderSidebar(this.records, selectedDate)}
        </div>
      </div>
    );
  };
}

export default Timeline;