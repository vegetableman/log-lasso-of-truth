var fs = require('fs');
var path = require('path');
var _ = require('lodash');
var moment = require('moment');
var uuid = require('uuid');

class Lasso {
  constructor() {
    this.dir = null;
    this.files = [];
    this.versions = [];
    this.logs = {};
    this.indexLogs = {};
  }

  parseDir(dir) {
    console.log('dir: ', dir);
    this.dir = dir;
    this.files = fs.readdirSync(dir);
    this.versions = [];
    for (const fname of this.files) {
      const match = fname.match(/^td-(.*)-index.html.log$/);
      if (!_.isEmpty(match)) {
        this.versions.push(match[1]);
      }
    }
    console.log("Versions found: ", this.versions);
    return this;
  }

  getVersions() {
    return this.versions;
  }

  getAllLogs(version) {
    const logs = {};
    for (const fname of this.files) {
      if (fname.indexOf(version) > -1) {
        if (!logs[version]) {
          logs[version] = {logs: []};
        }
        logs[version].logs.push(fname);
      }
    }
    return logs;
  }

  getIndexLogs(version) {
    this.logs = this.getAllLogs(version);
    this.indexLogs = _.filter(this.logs[version].logs, (l) => {
      return l.indexOf('-index.html.log') >= 0;
    });
    return this.indexLogs;
  }

  formatLog(acc, log) {
    let cacheDate = null;
    let logs = log.match(/(\{"name":"tddesktop",.*"v":0\})/g);
    _.each(logs, (l) => {
      const lObj = JSON.parse(l);
      const logDate = moment(lObj.time).format('YYYY-MM-DD');
      
      if ((!cacheDate || cacheDate !== logDate)) {
        if (!acc[logDate]) {
          acc[logDate] = [];
        }
        cacheDate = logDate;
      }

      lObj._logid = uuid();
      acc[cacheDate].push(lObj);
    });
    return acc;
  }

  parseIndexLogs(indexLogs) {
    let indexLogData = _.reduce(_.reverse(indexLogs), (acc, l) => {
      console.log(l);
      acc = this.formatLog(acc, fs.readFileSync(path.join(this.dir, l), 'utf8'));
      return acc;
    }, {});

    return {
      indexLogData,
    };
  }
}

module.exports = Lasso;