import _ from 'lodash';

const commaReplace = (data) => {
	return _.replace(data, /\'/g, '')
}

export default commaReplace;