let taskCounterInstance = null;

class TaskCounter {
	static getInstance() {
    if (taskCounterInstance) {
      return taskCounterInstance;
    }
    taskCounterInstance = new TaskCounter();
    return taskCounterInstance;
  }

	increment() {
		this.taskIndex++;
	}

	getIndex() {
		return this.taskIndex;
	}

	setOverflow(moverflow) {
		this.overflow = moverflow;
	}

	getOverflow() {
		return this.overflow;
	}

	reset() {
		this.taskIndex = 0;
	}
}

export default TaskCounter;