import _ from 'lodash';
import moment from 'moment';
import parseStringData from 'parse-string-data';

 const getNumberWithFixedLength = (num, len = 2, prefix = '0') => {
  let str = `${num}`;
  while (str.length < len) {
    str = `${prefix}${str}`;
  }
  return str;
}

const friendlyDuration = (ms, simplify, noSeconds) => {
  const duration = moment.duration(ms || 0);
  const timeArray = [];

  if (!simplify) {
    timeArray.push(getNumberWithFixedLength(Math.trunc(duration.asHours())));
    timeArray.push(getNumberWithFixedLength(duration.minutes()));
    if (!noSeconds) {
      timeArray.push(getNumberWithFixedLength(duration.seconds()));
    }
    return timeArray.join(':');
  }

  const hours = Math.trunc(duration.asHours());
  if (hours) {
    timeArray.push(`${hours}h`);
  }

  if (duration.minutes()) {
    timeArray.push(`${getNumberWithFixedLength(duration.minutes())}m`);
  } else {
    timeArray.push('0m');
  }

  if (!noSeconds) {
    timeArray.push(`${getNumberWithFixedLength(duration.seconds())}s`);
  }

  return timeArray.join(' ');
}

export default function aggregator(records) {
  let index = 0, runningTask = false;
  return _.reduce(records, (acc, value, key) => {
    const { msg, time } = value;
    const fmsg = msg.replace(/\n/g,'');

    if (fmsg.indexOf('Start Tracker!') > -1) {
      const parsedMsg = parseStringData(fmsg);
      if (_.get(parsedMsg, 'log__taskStart')) {
        if (runningTask) {
          runningTask.stop = { time };
          runningTask = null;

          if (acc.timeOrder && !_.last(acc.timeOrder).stop) {
            const lastTime = _.last(acc.timeOrder);
            lastTime.stop = time;
            lastTime.onAnotherStart = true;

            if (moment(time).format('h') !== moment(lastTime.start).format('h')) {
              const startHourTime = moment(time).startOf('hour');
              lastTime.overflow = {
                diff: moment(time).diff(startHourTime, 'minutes')
              };
            }
          }

          index++;
        }

        let taskId = null;

        if (!parsedMsg.taskId) {
          parsedMsg.taskId = 'dummy' + index;
          taskId = 'dummy' + index;
        } else {
          taskId = parsedMsg.taskId.replace(/\'/g, '') + '-' + index;
        }

        if (!acc[taskId]) {
          acc[taskId] = {};
        }

        acc[taskId].start = {...parsedMsg, time};

        if (!acc.taskOrder) {
          acc.taskOrder = [];
        }

        if (!acc.timeOrder) {
          acc.timeOrder = [];
        }        

        acc.taskOrder.push(taskId);
          
        const currentTime = moment(time);
      
        if (!_.isEmpty(acc.timeOrder)) {
          const lastStopTime = moment(_.last(acc.timeOrder).stop);
          if (currentTime.format('hh') === lastStopTime.format('hh')) {
            _.last(acc.timeOrder).nextDiff =  moment(time).diff(lastStopTime, 'minutes');
            acc.timeOrder.push({start: time});
          } 
          else {
            const startTime = moment(time).startOf('hour');
            if (startTime.diff(moment(currentTime)) < 0) {
              acc.timeOrder.push({start: time, prevDiff: moment(currentTime).diff(startTime, 'minutes')});
            }            
          }
        }
        else {
          const startTime = moment(time).startOf('hour');
          if (startTime.diff(moment(currentTime)) < 0) {
            acc.timeOrder.push({start: time, prevDiff: moment(currentTime).diff(startTime, 'minutes')});
          }
          else {
            acc.timeOrder.push({start: time});
          }
        }

        runningTask = acc[taskId];
      }
    } 
    
    else if (runningTask) {
      if (fmsg.indexOf('TimeMonitor.start') > -1 || fmsg.indexOf('get_companytime response') > -1) {
        if (!runningTask.companyTime) {
          runningTask.companyTime = [];
        }
        const parsedMsg = parseStringData(fmsg);
        runningTask.companyTime.push(_.get(parsedMsg, 'body', parsedMsg));
      }

      else if (fmsg.indexOf('ScreenshotMonitor.adjustScreenshotInterval') > -1) {
        const parsedMsg = parseStringData(fmsg);
        if (!runningTask.screenshots) {
          runningTask.screenshots = [];          
        }
        runningTask.screenshots.push({screenShotInterval: parsedMsg.screenshotIntervalMs/60000});
      }
      
      else if (fmsg.indexOf('ScreenshotMonitor.takeScreenshots: saved!') > -1) {
        _.last(runningTask.screenshots).info = parseStringData(fmsg);
        _.last(runningTask.screenshots).time = time;
      }
      
      else if (fmsg.indexOf('AppMonitor.tick') > -1) {
        const info = parseStringData(fmsg);
        info.time = time;
        if (!runningTask.appInfo) {
          runningTask.appInfo = [info];
        } else {
          runningTask.appInfo.push(info);
        }
      }
      
      else if (fmsg.indexOf('upload_screen') > -1) {
        const data = parseStringData(fmsg);
        if (!runningTask.uploadScreens) {
          runningTask.uploadScreens = [data];
        } else {
          runningTask.uploadScreens.push(data);
        }
      }

      else if (fmsg.indexOf('upload_timeuse') > -1) {
        const data = parseStringData(fmsg);
        if (!runningTask.uploadTimeUse) {
          runningTask.uploadTimeUse = [data];
        } else {
          runningTask.uploadTimeUse.push(data);
        }
      }

      else if (fmsg.indexOf('upload_time {') > -1) {
        const data = parseStringData(fmsg);
        if (_.get(runningTask, 'start.taskId') === 'dummy' + index) {
          const taskId = _.replace(_.last(_.get(data, 'requestData.task')), /\'/g, '');
          const taskIndex =  taskId + '-' + index;
          runningTask = Object.assign({}, acc['dummy' + index]);
          delete acc['dummy' + index];
          acc.taskOrder.pop();
          acc.taskOrder.push(taskIndex);
          acc[taskIndex] = runningTask;
          runningTask.start.taskId = _.last(_.get(data, 'requestData.task'));
        }
        if (!runningTask.uploadedTime) {
          runningTask.uploadedTime = [data];
        } else {
          runningTask.uploadedTime.push(data);
        }
      }

      else if (fmsg.indexOf('upload_time response') > -1) {
        const data = parseStringData(fmsg);
        _.last(runningTask.uploadedTime).responseData = data;
      }

      else if (fmsg.indexOf('get_all_tasks_time response') > -1) {
        const parsedMsg = parseStringData(fmsg);
        const data = _.get(parsedMsg, 'body', parsedMsg);
        const requestData = _.get(_.last(runningTask.uploadedTime), 'requestData');
        if (!requestData) {
          return acc;
        }
        const { start_time, end_time, end_tz } = requestData;
        const t_start_time = _.replace(_.last(start_time), /'/g, '');
        const t_end_time = _.replace(_.last(end_time), /'/g, '');
        const t_end_tz = _.replace(_.last(end_tz), /'/g, '');
        const startTime = moment(t_start_time + t_end_tz);
        const endTime = moment(t_start_time + t_end_tz);
        const startTimeMs = startTime.valueOf();
        const endTimeMs = endTime.valueOf();
        const lastUploadTimeMs = _.replace(data.last_upload_time, /'/g, '');
        const timeIndex = _.indexOf(_.get(data, 'task_id'), runningTask.start.taskId);
        const totalTime = _.replace(data.total_time[timeIndex], /'/g, '');

        const syncTimeMs = Math.min(Math.max(_.parseInt(lastUploadTimeMs, 10) * 1000, startTimeMs), endTimeMs);
        const logSecs = (endTimeMs - (syncTimeMs ? syncTimeMs : startTimeMs)) / 1000;
        const duration = (logSecs + _.parseInt(totalTime));

        data.friendlyDuration = friendlyDuration(duration * 1000);

        if (!runningTask.tasksTime) {
          runningTask.tasksTime = [data];
        } else {
          runningTask.tasksTime.push(data);
        }

        if (runningTask.hasStopped) {
          runningTask = null;
          index++;
        }
      }

     else if (fmsg.indexOf('timestat {') > -1) {
        const data = parseStringData(fmsg);
        if (!runningTask.timestat) {
          runningTask.timestat = [data];
        } else {
          runningTask.timestat.push(data);
        }
      }

      else if (fmsg.indexOf('timestat response') > -1) {
        const data = parseStringData(fmsg);
        _.last(runningTask.timestat).response = data;
      }

      else if (fmsg.indexOf('TimeMonitor.stop') > -1) {
        const parsedMsg = parseStringData(fmsg);
        if (_.get(parsedMsg, 'log__taskStop')) {
          if (acc[parsedMsg.taskId.replace(/\'/g, '') + '-'+ index]) {
            acc[parsedMsg.taskId.replace(/\'/g, '') + '-'+ index].stop = {...parsedMsg, time};
          } 
          const lastTime = _.last(acc.timeOrder)
          lastTime.stop = time;
          if (moment(time).format('h') !== moment(lastTime.start).format('h')) {
            const startHourTime = moment(time).startOf('hour');
            lastTime.overflow = {
              diff: moment(time).diff(startHourTime, 'minutes')
            };
          }
          runningTask.hasStopped = true;
        }
      }

      else if (fmsg.indexOf('PopupHelper.showCountdownPopup') > -1) {
        if (!runningTask.countdownpopup) {
          runningTask.countdownpopup = [];
        }
        const activity = runningTask.activity;
        const aggregratedActivity = _.reduce(activity, (acc, item, key) => {
          if (!acc.mouseMovements) {
            acc.mouseMovements = [];
          }
          acc.mouseMovements.push(_.get(item, 'details.mouseMovements'));
          if (!acc.mouseClicks) {
            acc.mouseClicks = [];
          }
          acc.mouseClicks.push(_.get(item, 'details.mouseClicks'));
          if (!acc.keyStrokes) {
            acc.keyStrokes = [];
          }
          acc.keyStrokes.push(_.get(item, 'details.keyStrokes'));
          if (!acc.activityFactor) {
            acc.activityFactor = [];
          }
          acc.activityFactor.push(_.get(item, 'details.activityFactor'));
          if (!acc.times) {
            acc.times = [];
          }
          acc.times.push(_.get(item, 'time'));
          return acc;
        }, {});
        runningTask.countdownpopup.push({showPopup: time, activity: aggregratedActivity});
      }

      else if (fmsg.indexOf('PopupCountdown.onTimeOut') > -1) {
        const parsedMsg = parseStringData(fmsg);
        const countdownpopup = _.last(runningTask.countdownpopup);
        countdownpopup.selectedTask = parsedMsg;
        countdownpopup.onTimeOut = time;
      }

      else if (fmsg.indexOf('PopupCountdown.onStillWorking') > -1) {
        const countdownpopup = _.last(runningTask.countdownpopup);
        countdownpopup.onStillWorking = time;
      }

      else if (fmsg.indexOf('PopupHelper.showWelcomeBackPopup') > -1) {
        if (!runningTask.welcomebackpopup) {
          runningTask.welcomebackpopup = [];
        }
        runningTask.welcomebackpopup.push({showPopup: time});
      }

      else if (fmsg.indexOf('PopupWelcomeBackManual.onWasWorking') > -1) {
        const parsedMsg = parseStringData(fmsg);
        const welcomebackpopup = _.last(runningTask.welcomebackpopup);
        welcomebackpopup.onWasWorking = time;
        welcomebackpopup.selectedTask = parsedMsg;
      }

      else if (fmsg.indexOf('PopupWelcomeBackNoManual.onStartWorking') > -1) {
        const parsedMsg = parseStringData(fmsg);
        const welcomebackpopup = _.last(runningTask.welcomebackpopup);
        welcomebackpopup.onStartWorking = time;
        welcomebackpopup.selectedTask = parsedMsg;
      }

      else if (fmsg.indexOf('DeviceMonitor.reportTick') > -1) {
        const parsedMsg = parseStringData(fmsg);
        if (!runningTask.activity) {
          runningTask.activity = [];
        }
        const activityQueue = _.get(parsedMsg, 'activityQueue');
        let activityQueueArr = _.split(activityQueue, '-');
        let mouseMovements = _.nth(activityQueueArr, 0);
        let mouseClicks = _.nth(activityQueueArr, 1);
        let keyStrokes = _.nth(activityQueueArr, 2);
        let activityFactor = _.nth(activityQueueArr, 3);
        runningTask.activity.push({details: { mouseMovements: _.replace(mouseMovements, /\'/g, ''), mouseClicks, keyStrokes, activityFactor}, time});
      }

      else if (fmsg.indexOf('PopupWelcomeBackManual.onWasBreak') > -1) {
        const parsedMsg = parseStringData(fmsg);
        const welcomebackpopup = _.last(runningTask.welcomebackpopup);
        welcomebackpopup.onWasBreak = time;
        welcomebackpopup.selectedTask = parsedMsg;
      }

      else if (fmsg.indexOf('TimeMonitor.onPowerSuspend') > -1) {
        const parsedMsg = parseStringData(fmsg);
        if (!runningTask.power) {
          runningTask.power = [];
        }
        runningTask.power.push({suspend: parsedMsg, suspendTime: time});
      }

      else if (fmsg.indexOf('TimeMonitor.onPowerResume') > -1) {
        const parsedMsg = parseStringData(fmsg);
        _.last(runningTask).power.push({resume: parsedMsg, resumeTime: time});
      }

      else if (fmsg.indexOf('PopupWelcomeBackManual.onContinueBreak') > -1) {
        const parsedMsg = parseStringData(fmsg);
        const welcomebackpopup = _.last(runningTask.welcomebackpopup);
        welcomebackpopup.onWasBreak = time;
        welcomebackpopup.selectedTask = parsedMsg;
      }
    }

    else if (fmsg.indexOf('Timer.onPowerSuspend') > -1 && fmsg.indexOf("Global") > -1) {
      if (!acc.power) {
        acc.power = [];
      }
      const parsedMsg = parseStringData(fmsg);
      acc['power'].push({suspend: parsedMsg, suspendTime: time});
    }

    else if (fmsg.indexOf('Timer.onPowerResume') > -1) {
      const parsedMsg = parseStringData(fmsg);
      if (!acc['power']) acc['power'] = [];
      _.last(acc['power']).resume = parsedMsg;
      _.last(acc['power']).resumeTime = time;
    }

    return acc;
  }, {});
}

